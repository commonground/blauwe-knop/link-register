package linkregister

import (
	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"
	"go.uber.org/zap"
)

type LinkTokenRepository interface {
	Get(debtRequestID string) (*LinkToken, error)
	Update(debtRequestID string, linkToken LinkToken) error
	AssignBSN(bsn string, linkToken LinkToken) error
	GetBSNFromLinkToken(linkToken LinkToken) (*BSN, error)
	healthcheck.Checker
}
type TokenGenerator interface {
	GenerateLinkToken() LinkToken
}

type LinkTokenUseCase struct {
	linkTokenRepository LinkTokenRepository
	tokenGenerator      TokenGenerator
	logger              *zap.Logger
}

func NewLinkTokenUseCase(logger *zap.Logger, linkTokenRepository LinkTokenRepository, tokenGenerator TokenGenerator) *LinkTokenUseCase {
	return &LinkTokenUseCase{
		linkTokenRepository: linkTokenRepository,
		tokenGenerator:      tokenGenerator,
		logger:              logger,
	}
}

func (a *LinkTokenUseCase) Get(debtRequestID string) (*LinkToken, error) {
	return a.linkTokenRepository.Get(debtRequestID)
}

func (a *LinkTokenUseCase) Update(debtRequestID string) (LinkToken, error) {
	linkToken := a.tokenGenerator.GenerateLinkToken()
	return linkToken, a.linkTokenRepository.Update(debtRequestID, linkToken)
}

func (a *LinkTokenUseCase) AssignBSN(bsn string, linkToken LinkToken) error {
	return a.linkTokenRepository.AssignBSN(bsn, linkToken)
}

func (a *LinkTokenUseCase) GetBSNFromLinkToken(linkToken LinkToken) (*BSN, error) {
	return a.linkTokenRepository.GetBSNFromLinkToken(linkToken)
}

func (a *LinkTokenUseCase) GetHealthChecks() []healthcheck.Checker {
	return []healthcheck.Checker{
		a.linkTokenRepository,
	}
}
