# Use go 1.x based on alpine image.
FROM golang:1.19.4-alpine AS build

ADD . /go/src/link-register/
ENV GO111MODULE on
WORKDIR /go/src/link-register
RUN go mod download
RUN go build -o dist/bin/link-register ./cmd/link-register

# Release binary on latest alpine image.
FROM alpine:latest

COPY --from=build /go/src/link-register/dist/bin/link-register /usr/local/bin/link-register

# Add non-priveleged user
RUN adduser -D -u 1001 appuser
USER appuser
CMD ["/usr/local/bin/link-register"]
