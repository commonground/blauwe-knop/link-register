// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http

import (
	"context"
	"log"
	"net/http"

	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"

	"linkregister"
)

type linkTokenPayloadResponse struct {
	LinkToken linkregister.LinkToken `json:"linkToken"`
}
type TokenGenerator interface {
	GenerateLinkToken() linkregister.LinkToken
}

type key int

const (
	linkTokenUseCaseKey key = iota
)

func NewRouter(linkTokenUseCase *linkregister.LinkTokenUseCase, apiKey string) *chi.Mux {
	r := chi.NewRouter()

	r.Use(middleware.Logger)

	if apiKey != "" {
		r.Use(authenticatedOnly(apiKey))
	}

	r.Route("/link-tokens", func(r chi.Router) {
		r.Get("/", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), linkTokenUseCaseKey, linkTokenUseCase)
			handlerGetLinkToken(w, r.WithContext(ctx))
		})
		r.Post("/", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), linkTokenUseCaseKey, linkTokenUseCase)
			handlerCreateLinkToken(w, r.WithContext(ctx))
		})
		r.Put("/{linkToken}", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), linkTokenUseCaseKey, linkTokenUseCase)
			handlerAssignBSNToLinkToken(w, r.WithContext(ctx))
		})
		r.Get("/{linkToken}", func(w http.ResponseWriter, r *http.Request) {
			ctx := context.WithValue(r.Context(), linkTokenUseCaseKey, linkTokenUseCase)
			handlerGetBSNFromLinkToken(w, r.WithContext(ctx))
		})
	})

	healthCheckHandler := healthcheck.NewHandler("link-register", linkTokenUseCase.GetHealthChecks())
	r.Route("/health", func(r chi.Router) {
		r.Get("/", healthCheckHandler.HandleHealth)
		r.Get("/check", healthCheckHandler.HandlerHealthCheck)
	})

	return r
}

func authenticatedOnly(apiKey string) func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			providedApiKey := r.Header.Get("Authentication")

			if providedApiKey != apiKey {
				log.Printf("Unauthorized: apikey unvalid")
				http.Error(w, http.StatusText(http.StatusUnauthorized), http.StatusUnauthorized)
				return
			}
			next.ServeHTTP(w, r)
		})
	}
}
