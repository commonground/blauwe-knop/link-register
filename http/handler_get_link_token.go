package http

import (
	"encoding/json"
	"log"
	"net/http"

	"linkregister"
)

func handlerGetLinkToken(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	repository, _ := ctx.Value(linkTokenUseCaseKey).(*linkregister.LinkTokenUseCase)

	debtRequestId := r.URL.Query().Get("debtRequestId")
	if debtRequestId == "" {
		log.Printf("a debt request id is required")
		http.Error(w, "a debt request id is required (specify as ?debtRequestId=)", http.StatusBadRequest)
		return
	}

	linkToken, err := repository.Get(debtRequestId)
	if err != nil {
		log.Printf("failed to retrieve link token: %v", linkToken)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	if linkToken == nil {
		log.Printf("could not find link token for debt request")
		http.Error(w, "could not find link token for debt request", http.StatusNotFound)
		return
	}

	responsePayload := linkTokenPayloadResponse{
		LinkToken: *linkToken,
	}

	err = json.NewEncoder(w).Encode(responsePayload)
	if err != nil {
		log.Printf("failed to encode response payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
}
