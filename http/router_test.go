package http_test

import (
	"fmt"
	http_infra "linkregister/http"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"

	"github.com/golang/mock/gomock"

	"linkregister"
	"linkregister/mock"
)

const DummyDebtRequestID = "dummy-debt-request-id"
const DummyBSN = "dummy-bsn"

var DummyLinkToken = linkregister.LinkToken("dummy-link-token")

func generateLinkTokenRepository(t *testing.T) *mock.MockLinkTokenRepository {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	repo := mock.NewMockLinkTokenRepository(ctrl)
	return repo
}
func generateTokenGenerator(t *testing.T) *mock.MockTokenGenerator {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	repo := mock.NewMockTokenGenerator(ctrl)
	return repo
}

func Test_CreateRouter_Authentication(t *testing.T) {
	logger := zap.NewNop()

	type fields struct {
		linkTokenRepository linkregister.LinkTokenRepository
		tokenGenerator      http_infra.TokenGenerator
		apiKey              string
	}
	type args struct {
		debtRequestId string
		apiKey        string
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
	}{
		{
			"authentication disabled",
			fields{
				linkTokenRepository: func() linkregister.LinkTokenRepository {
					repo := generateLinkTokenRepository(t)
					repo.EXPECT().Get(DummyDebtRequestID).Return(&DummyLinkToken, nil).AnyTimes()
					return repo
				}(),
				tokenGenerator: func() http_infra.TokenGenerator {
					repo := generateTokenGenerator(t)
					return repo
				}(),
				apiKey: "",
			},
			args{
				debtRequestId: DummyDebtRequestID,
			},
			http.StatusOK,
		},
		{
			"authentication enabled - with incorrect key provided",
			fields{
				linkTokenRepository: generateLinkTokenRepository(t),
				tokenGenerator:      generateTokenGenerator(t),
				apiKey:              "dummy-api-key",
			},
			args{
				debtRequestId: DummyDebtRequestID,
				apiKey:        "incorrect-key",
			},
			http.StatusUnauthorized,
		},
		{
			"authentication enabled - with correct key provided",
			fields{
				linkTokenRepository: func() linkregister.LinkTokenRepository {
					repo := generateLinkTokenRepository(t)
					repo.EXPECT().Get(DummyDebtRequestID).Return(&DummyLinkToken, nil).AnyTimes()
					return repo
				}(),
				tokenGenerator: func() http_infra.TokenGenerator {
					repo := generateTokenGenerator(t)
					return repo
				}(),
				apiKey: "dummy-api-key",
			},
			args{
				debtRequestId: DummyDebtRequestID,
				apiKey:        "dummy-api-key",
			},
			http.StatusOK,
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router := http_infra.NewRouter(linkregister.NewLinkTokenUseCase(logger, test.fields.linkTokenRepository, test.fields.tokenGenerator), test.fields.apiKey)
			w := httptest.NewRecorder()

			url := fmt.Sprintf("/link-tokens/?debtRequestId=%s", test.args.debtRequestId)
			request := httptest.NewRequest("GET", url, nil)

			if test.args.apiKey != "" {
				request.Header.Set("Authentication", test.args.apiKey)
			}

			router.ServeHTTP(w, request)
			resp := w.Result()

			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
		})
	}
}
