// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_test

import (
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"

	"linkregister"
	http_infra "linkregister/http"
)

func Test_CreateRouter_GetLinkToken(t *testing.T) {
	logger := zap.NewNop()
	type fields struct {
		linkTokenRepository linkregister.LinkTokenRepository
		tokenGenerator      http_infra.TokenGenerator
	}
	type args struct {
		debtRequestId string
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
		expectedBody           string
	}{
		{
			"for an empty debt request id",
			fields{
				linkTokenRepository: func() linkregister.LinkTokenRepository {
					repo := generateLinkTokenRepository(t)
					repo.EXPECT().Get(gomock.Any()).Return(nil, nil).AnyTimes()
					return repo
				}(),
				tokenGenerator: generateTokenGenerator(t),
			},
			args{
				debtRequestId: "",
			},
			http.StatusBadRequest,
			"a debt request id is required (specify as ?debtRequestId=)\n",
		},
		{
			"failed to retrieve link token",
			fields{
				linkTokenRepository: func() linkregister.LinkTokenRepository {
					repo := generateLinkTokenRepository(t)
					repo.EXPECT().Get(gomock.Any()).Return(nil, errors.New("arbitrary error")).AnyTimes()
					return repo
				}(),
				tokenGenerator: generateTokenGenerator(t),
			},
			args{
				debtRequestId: DummyDebtRequestID,
			},
			http.StatusInternalServerError,
			"internal error\n",
		},
		{
			"link token does not exist",
			fields{
				linkTokenRepository: func() linkregister.LinkTokenRepository {
					repo := generateLinkTokenRepository(t)
					repo.EXPECT().Get(DummyDebtRequestID).Return(nil, nil).AnyTimes()
					return repo
				}(),
				tokenGenerator: func() http_infra.TokenGenerator {
					repo := generateTokenGenerator(t)
					return repo
				}(),
			},
			args{
				debtRequestId: DummyDebtRequestID,
			},
			http.StatusNotFound,
			"could not find link token for debt request\n",
		},
		{
			"happy flow",
			fields{
				linkTokenRepository: func() linkregister.LinkTokenRepository {
					repo := generateLinkTokenRepository(t)
					repo.EXPECT().Get(DummyDebtRequestID).Return(&DummyLinkToken, nil).AnyTimes()
					return repo
				}(),
				tokenGenerator: func() http_infra.TokenGenerator {
					repo := generateTokenGenerator(t)
					return repo
				}(),
			},
			args{
				debtRequestId: DummyDebtRequestID,
			},
			http.StatusOK,
			"{\"linkToken\":\"dummy-link-token\"}\n",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router := http_infra.NewRouter(linkregister.NewLinkTokenUseCase(logger, test.fields.linkTokenRepository, test.fields.tokenGenerator), "")
			w := httptest.NewRecorder()

			url := fmt.Sprintf("/link-tokens?debtRequestId=%s", test.args.debtRequestId)
			request := httptest.NewRequest("GET", url, nil)

			router.ServeHTTP(w, request)

			resp := w.Result()
			body, _ := ioutil.ReadAll(resp.Body)

			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
			assert.Equal(t, test.expectedBody, string(body))
		})
	}
}
