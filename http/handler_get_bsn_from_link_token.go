package http

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/go-chi/chi"

	"linkregister"
)

type getBSNFromLinkTokenResponsePayload struct {
	BSN string `json:"bsn"`
}

func handlerGetBSNFromLinkToken(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	repository, _ := ctx.Value(linkTokenUseCaseKey).(*linkregister.LinkTokenUseCase)

	linkTokenFromRoute := chi.URLParam(r, "linkToken")
	linkToken := linkregister.LinkToken(linkTokenFromRoute)

	if linkToken == "" {
		log.Printf("a link token is required")
		http.Error(w, "a link token is required", http.StatusMethodNotAllowed)
		return
	}
	bsn, err := repository.GetBSNFromLinkToken(linkToken)
	if err != nil {
		log.Printf("failed to retrieve bsn to link token: %v", err)
		http.Error(w, "internal server error", http.StatusInternalServerError)
		return
	}

	if bsn == nil {
		log.Printf("bsn not found")
		http.Error(w, "not found", http.StatusNotFound)
		return
	}

	responsePayload := getBSNFromLinkTokenResponsePayload{
		BSN: bsn.ToString(),
	}

	err = json.NewEncoder(w).Encode(responsePayload)
	if err != nil {
		log.Printf("failed to encode response payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
}
