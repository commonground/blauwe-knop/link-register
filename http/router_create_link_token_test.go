// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_test

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"

	"linkregister"
	http_infra "linkregister/http"
)

func Test_CreateRouter_CreateLinkToken(t *testing.T) {
	logger := zap.NewNop()

	type fields struct {
		linkTokenRepository linkregister.LinkTokenRepository
		tokenGenerator      http_infra.TokenGenerator
	}
	type args struct {
		debtRequestId string
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
		expectedBody           string
	}{
		{
			"happy flow",
			fields{
				linkTokenRepository: func() linkregister.LinkTokenRepository {
					repo := generateLinkTokenRepository(t)
					repo.EXPECT().Update(DummyDebtRequestID, gomock.Any()).Return(nil).AnyTimes()
					return repo
				}(),
				tokenGenerator: func() http_infra.TokenGenerator {
					repo := generateTokenGenerator(t)
					repo.EXPECT().GenerateLinkToken().Return(DummyLinkToken).AnyTimes()
					return repo
				}(),
			},
			args{
				debtRequestId: DummyDebtRequestID,
			},
			http.StatusOK,
			"{\"linkToken\":\"dummy-link-token\"}\n",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router := http_infra.NewRouter(linkregister.NewLinkTokenUseCase(logger, test.fields.linkTokenRepository, test.fields.tokenGenerator), "")
			w := httptest.NewRecorder()

			requestBody := struct {
				DebtRequestID string `json:"debtRequestId"`
			}{
				DebtRequestID: test.args.debtRequestId,
			}
			requestBodyAsJSON, _ := json.Marshal(requestBody)
			request := httptest.NewRequest("POST", "/link-tokens", bytes.NewReader(requestBodyAsJSON))

			router.ServeHTTP(w, request)

			resp := w.Result()
			body, _ := ioutil.ReadAll(resp.Body)

			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
			assert.Equal(t, test.expectedBody, string(body))
		})
	}
}
