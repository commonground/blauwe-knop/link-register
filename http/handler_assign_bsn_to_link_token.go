package http

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"github.com/go-chi/chi"

	"linkregister"
)

type assignBSNToLinkTokenRequestPayload struct {
	BSN string `json:"bsn"`
}

func handlerAssignBSNToLinkToken(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	linkTokenUsecase, _ := ctx.Value(linkTokenUseCaseKey).(*linkregister.LinkTokenUseCase)

	linkTokenFromRoute := chi.URLParam(r, "linkToken")
	linkToken := linkregister.LinkToken(linkTokenFromRoute)

	if linkToken == "" {
		log.Printf("a link token is required")
		http.Error(w, "a link token is required", http.StatusBadRequest)
		return
	}

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("failed to read request payload: %v", err)
		http.Error(w, "failed to read request payload", http.StatusBadRequest)
		return
	}

	var requestPayload assignBSNToLinkTokenRequestPayload
	err = json.Unmarshal(body, &requestPayload)
	if err != nil {
		log.Printf("invalid json provided: %v", err)
		http.Error(w, "invalid json provided", http.StatusBadRequest)
		return
	}

	if requestPayload.BSN == "" {
		log.Printf("a bsn is required")
		http.Error(w, "a bsn is required", http.StatusBadRequest)
		return
	}

	err = linkTokenUsecase.AssignBSN(requestPayload.BSN, linkToken)
	if err != nil {
		log.Printf("failed to assign bsn to link token: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	_, err = w.Write([]byte("OK\n"))
	if err != nil {
		log.Printf("failed to write response: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
}
