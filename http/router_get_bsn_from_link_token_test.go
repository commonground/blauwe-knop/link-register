// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package http_test

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/stretchr/testify/assert"
	"go.uber.org/zap"

	"linkregister"
	http_infra "linkregister/http"
)

func Test_CreateRouter_GetBSNFromLinkToken(t *testing.T) {
	logger := zap.NewNop()

	type fields struct {
		linkTokenRepository linkregister.LinkTokenRepository
		tokenGenerator      http_infra.TokenGenerator
	}
	type args struct {
		bsn       string
		linkToken string
	}
	tests := []struct {
		name                   string
		fields                 fields
		args                   args
		expectedHTTPStatusCode int
		expectedBody           string
	}{
		{
			"failed to retrieve bsn to link token",
			fields{
				linkTokenRepository: func() linkregister.LinkTokenRepository {
					repo := generateLinkTokenRepository(t)
					repo.EXPECT().GetBSNFromLinkToken(DummyLinkToken).Return(nil, nil).AnyTimes()
					return repo
				}(),
				tokenGenerator: generateTokenGenerator(t),
			},
			args{
				bsn:       DummyBSN,
				linkToken: string(DummyLinkToken),
			},
			http.StatusNotFound,
			"not found\n",
		},
		{
			"happy flow",
			fields{
				linkTokenRepository: func() linkregister.LinkTokenRepository {
					repo := generateLinkTokenRepository(t)
					repo.EXPECT().GetBSNFromLinkToken(DummyLinkToken).Return(linkregister.NewBSN("123456789")).AnyTimes()
					return repo
				}(),
				tokenGenerator: generateTokenGenerator(t),
			},
			args{
				bsn:       DummyBSN,
				linkToken: string(DummyLinkToken),
			},
			http.StatusOK,
			"{\"bsn\":\"123456789\"}\n",
		},
	}

	for _, test := range tests {
		t.Run(test.name, func(t *testing.T) {
			router := http_infra.NewRouter(linkregister.NewLinkTokenUseCase(logger, test.fields.linkTokenRepository, test.fields.tokenGenerator), "")
			w := httptest.NewRecorder()

			requestBody := struct {
				BSN string `json:"bsn"`
			}{
				BSN: test.args.bsn,
			}
			requestBodyAsJSON, _ := json.Marshal(requestBody)

			log.Printf("lt: %s", test.args.linkToken)
			url := fmt.Sprintf("/link-tokens/%s", test.args.linkToken)
			request := httptest.NewRequest(http.MethodGet, url, bytes.NewReader(requestBodyAsJSON))

			router.ServeHTTP(w, request)

			resp := w.Result()
			body, _ := ioutil.ReadAll(resp.Body)

			assert.Equal(t, test.expectedHTTPStatusCode, resp.StatusCode)
			assert.Equal(t, test.expectedBody, string(body))
		})
	}
}
