package http

import (
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"

	"linkregister"
)

type createLinkTokenRequestPayload struct {
	DebtRequestId string `json:"debtRequestId"`
}

type createLinkTokenResponsePayload struct {
	LinkToken string `json:"linkToken"`
}

func handlerCreateLinkToken(w http.ResponseWriter, r *http.Request) {
	ctx := r.Context()
	linkTokenUsecase, _ := ctx.Value(linkTokenUseCaseKey).(*linkregister.LinkTokenUseCase)

	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		log.Printf("failed to read request payload: %v", err)
		http.Error(w, "failed to read request payload", http.StatusBadRequest)
		return
	}

	var requestPayload createLinkTokenRequestPayload
	err = json.Unmarshal(body, &requestPayload)
	if err != nil {
		log.Printf("invalid json provided: %v", err)
		http.Error(w, "invalid json provided", http.StatusBadRequest)
		return
	}

	if len(requestPayload.DebtRequestId) < 1 {
		log.Printf("a debt request id is required")
		http.Error(w, "a debt request id is required", http.StatusBadRequest)
		return
	}

	linkToken, err := linkTokenUsecase.Update(requestPayload.DebtRequestId)
	if err != nil {
		log.Printf("failed to update link token for debt request: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}

	responsePayload := createLinkTokenResponsePayload{
		LinkToken: string(linkToken),
	}

	err = json.NewEncoder(w).Encode(responsePayload)
	if err != nil {
		log.Printf("failed to encode response payload: %v", err)
		http.Error(w, "internal error", http.StatusInternalServerError)
		return
	}
}
