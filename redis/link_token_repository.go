// Copyright © VNG Realisatie 2020
// Licensed under the EUPL

package redis

import (
	"fmt"
	"log"
	"math/rand"
	"time"

	"github.com/go-redis/redis/v7"
	healthcheck "gitlab.com/commonground/blauwe-knop/health-checker/healthchecker"

	"linkregister"
)

const LinkTokenKeyPrefix = "link-token"

type LinkTokenRepository struct {
	client *redis.Client
}

func NewLinkTokenRepository(redisDSN string) (*LinkTokenRepository, error) {
	options, err := redis.ParseURL(redisDSN)
	if err != nil {
		return nil, err
	}

	client := redis.NewClient(options)

	_, err = client.Ping().Result()
	if err != nil {
		return nil, err
	}
	rand.Seed(time.Now().UnixNano())
	return &LinkTokenRepository{
		client: client,
	}, nil
}

func (r *LinkTokenRepository) Get(debtRequestID string) (*linkregister.LinkToken, error) {
	var result linkregister.LinkToken
	linkToken, err := r.client.Get(fmt.Sprintf("%s-%s", LinkTokenKeyPrefix, debtRequestID)).Result()
	if err != nil && err != redis.Nil {
		return nil, fmt.Errorf("failed to fetch link token: %v", err)
	}

	if err != nil && err == redis.Nil {
		return nil, nil
	}

	result = linkregister.LinkToken(linkToken)
	return &result, nil
}

func (r *LinkTokenRepository) Update(debtRequestID string, linkToken linkregister.LinkToken) error {
	_, err := r.client.Set(fmt.Sprintf("%s-%s", LinkTokenKeyPrefix, debtRequestID), string(linkToken), 0).Result()
	if err != nil {
		fmt.Printf("failed to assign linkToken to request id: %v", err)
		return err
	}

	return nil
}

func (r *LinkTokenRepository) AssignBSN(bsn string, linkToken linkregister.LinkToken) error {
	_, err := r.client.Set(fmt.Sprintf("%s-%s", LinkTokenKeyPrefix, linkToken), bsn, 0).Result()
	if err != nil {
		fmt.Printf("failed to assign bsn to link token: %v", err)
		return err
	}

	return nil
}

func (d *LinkTokenRepository) GetBSNFromLinkToken(linkToken linkregister.LinkToken) (*linkregister.BSN, error) {
	bsnForLinkToken, err := d.client.Get(fmt.Sprintf("%s-%s", LinkTokenKeyPrefix, linkToken)).Result()
	if err != nil {
		if err == redis.Nil {
			return nil, nil
		}
		return nil, fmt.Errorf("failed to fetch bsn for linktoken: %v", err)
	}

	return linkregister.NewBSN(bsnForLinkToken)
}

func (s *LinkTokenRepository) GetHealthCheck() healthcheck.Result {
	name := "redis"
	status := healthcheck.StatusOK
	start := time.Now()

	pong, err := s.client.Ping().Result()
	if err != nil {
		log.Printf("ping to redis failed: %v", err)
		status = healthcheck.StatusError
	} else {
		log.Printf("ping to redis successful: %s", pong)
	}

	return healthcheck.Result{
		Name:         name,
		Status:       status,
		ResponseTime: time.Since(start).Seconds(),
		HealthChecks: nil,
	}
}
